# README #

### What is this repository for? ###

This game was built for my second project in the Interactive Studio course taught by Charles Palmer and Lisa Brown at Harrisburg University of Science and Technology. Our task was to build an experimental game using some form of multiplayer interaction. Originally, we planned on using Unity's networking system to implement a network enabled multiplayer solution, but given our time constraints, it was not feasible. So we ended up making the game split screen. 

For this project, the team was comprised of myself as the lead developer, Zach George the 3D artist/animator, and Anthony DelSordo the texture artist and second developer. 

### How do I get set up? ###

The game executable can be found in the downloads section of this repository!