﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObjectiveScript : MonoBehaviour {

	[SerializeField] TextAsset objectiveList;
	private string[] objectives;
	private string currentObjective;
	private int count = 0;
	public bool objectiveTripped = false;

	void Start () {
		objectives = objectiveList.text.Split("\n".ToCharArray(), 99999);
		currentObjective = objectives [count];
	}

	void Update() {
		gameObject.GetComponentInChildren<Text> ().text = "Objective: " + objectives[count];
	}

	public void IncrementObjectives () {
		if (!objectiveTripped)
			count += 2;
		objectiveTripped = true;
	}
}
