﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerNetworkSetup : NetworkBehaviour {

	enum PlayerType { Stealth, Overwatch };
	[SerializeField] PlayerType type;
	[SerializeField] GameObject stealthPlayer, overwatchPlayer;

	void Start () {
		if (isLocalPlayer) {
			if (type == PlayerType.Stealth) {
				stealthPlayer.SetActive (true);
			} else if (type == PlayerType.Overwatch) {
				overwatchPlayer.SetActive (true);
			}
		}
	}
}
