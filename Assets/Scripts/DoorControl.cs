﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DoorControl : MonoBehaviour {

	[SerializeField] private bool activate;
	[SerializeField] private Camera overwatchCamera;
	[SerializeField] private Canvas canvas;
	Animator animator;
	Button doorButton;
	NavMeshObstacle navmeshObstacle;

	void Start () {
		animator = GetComponent<Animator> ();
		navmeshObstacle = GetComponent<NavMeshObstacle>();
		doorButton = GetComponentInChildren<Button> ();
		if (canvas.gameObject.activeInHierarchy) {
			SetCamera ();
		}
	}

	void Update() {
		if (activate) {
			animator.SetBool ("Moving", true);
			activate = false;
		}
	}

	public void ActivateControl () {
		activate = true;
	}

	public void DoorOpen() {
		doorButton.GetComponent<Image> ().color = Color.green;
		navmeshObstacle.enabled = false;
		animator.SetBool ("Moving", false);
		animator.SetBool ("Closed", false);
	}

	public void DoorClosed() {
		doorButton.GetComponent<Image> ().color = Color.red;
		navmeshObstacle.enabled = true;
		animator.SetBool ("Moving", false);
		animator.SetBool ("Closed", true);
	}

	void OnCollisionEnter (Collision other) {
		if (other.collider.tag == "Guard") {
			activate = true;
		}
	}

	public void SetCamera () {
		canvas.worldCamera = GameObject.FindGameObjectWithTag("Overwatch").GetComponent<OverwatchController>().cam;
	}
}
