﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	[SerializeField] bool cursorLocked;
	private bool firstTerminal, secondTerminal;
	private int countOfTerminals;

	void Start () {
		if (cursorLocked) {
			Cursor.lockState = CursorLockMode.Confined;
			//Cursor.visible = true;
		}
	}

	#region Properties
	public bool FirstTerminal {
		get {
			return firstTerminal;
		}

		set {
			firstTerminal = value;
		}
	}

	public bool SecondTerminal {
		get {
			return secondTerminal;
		}

		set {
			secondTerminal = value;
		}
	}

	public int CountOfTerminals {
		get {
			return countOfTerminals;
		}

		set {
			countOfTerminals = value;
		}
	}
	#endregion
}
