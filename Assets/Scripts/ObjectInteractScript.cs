﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof (BoxCollider))]
public class ObjectInteractScript : MonoBehaviour {
	public enum InteractionType { None, Terminal };
	[SerializeField] InteractionType interaction;
	MultiPlatformInput inputController;
	int count = 1;

	[SerializeField] private GameObject[] objectsToAffect;

	void Start() {
		inputController = new MultiPlatformInput ();
	}

	void OnTriggerStay (Collider other) {
		if (other.tag == "Player" && inputController.GetButtonDown("Interact")) {
			switch (interaction) {
			case InteractionType.Terminal:
				TerminalInteract ();
				if (!GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().FirstTerminal)
					GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().FirstTerminal = true;
				else if (GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().FirstTerminal && 
					!GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().SecondTerminal &&
					GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().CountOfTerminals > 0) {
					GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().SecondTerminal = true;
				}
				GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().CountOfTerminals++;

				if (GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().CountOfTerminals > 2)
					GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<ObjectiveScript>().IncrementObjectives();
				break;
			default:
				break;
			}
		}
	}

	void TerminalInteract () {
		foreach (GameObject obj in objectsToAffect) {
			if (obj.GetComponentInChildren<Button> () != null) {
				obj.GetComponentInChildren<Button> ().interactable = true;
			}

			if (obj.GetComponent<DoorControl> () != null) {
				obj.GetComponent<DoorControl> ().SetCamera ();
			}
		}
	}
}
