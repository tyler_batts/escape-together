﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class Screenshot : MonoBehaviour
{
	[SerializeField] private bool stopCapture, startCapture;
	[SerializeField] private int maxFrames;
	private bool capturing;
	private int count = 0;

	void Update()
	{
		if (stopCapture || Input.GetKeyDown(KeyCode.Escape) || count > maxFrames) {
			stopCapture = false;
			capturing = false;
		}

		if (startCapture || Input.GetKeyDown(KeyCode.KeypadEnter)) {
			startCapture = false;
			capturing = true;
		}
			

	}

	void FixedUpdate() {
		StartCoroutine(ScreenshotEncode());
	}

	IEnumerator ScreenshotEncode()
	{
		if (capturing) {
			// wait for graphics to render
			yield return new WaitForEndOfFrame();

			// create a texture to pass to encoding
			Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

			// put buffer into texture
			texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
			texture.Apply();

			// split the process up--ReadPixels() and the GetPixels() call inside of the encoder are both pretty heavy
			yield return 0;

			byte[] bytes = texture.EncodeToPNG();

			// save our test image (could also upload to WWW)
			//Application.CaptureScreenshot("C:/Users/Tyler/Google Drive/Project Black Hole/Screenshots/testscreen-" + count + ".png", 1);

			string path = "game_Data/Resources/testscreen-" + count + ".png";;
			#if (UNITY_EDITOR)
			path = "Assets/Resources/Output/testscreen-" + count + ".png";
			#endif

			File.WriteAllBytes(path, bytes);
			count++;

			// Added by Karl. - Tell unity to delete the texture, by default it seems to keep hold of it and memory crashes will occur after too many screenshots.
			DestroyObject( texture );

			//Debug.Log( Application.dataPath + "/Screenshots/testscreen-" + count + ".png" );
		}
	}
}