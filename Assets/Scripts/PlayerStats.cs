﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerStats {
	public float runSpeed;
	public float sprintSpeed;
	public float jumpSpeed;
}
