﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OverwatchController : MonoBehaviour {

	public Camera cam, staticCam;
	[SerializeField] float cameraSizeMin, cameraSizeMax;
	Vector3 mousePos, previousMousePos, mousePosDelta;

	[SerializeField] GameObject securityCamDisplayPrefab;
	[SerializeField] float camDisplayBuffer;
	Canvas canvas;
	RawImage bigPlane;
	RenderTexture[] renderTextures;
	Camera[] securityCameras;

	[SerializeField] GameObject[] turnOnAtStart;

	void Start () {
		foreach (GameObject obj in turnOnAtStart) {
			obj.SetActive (true);
		}
		canvas = GetComponentInChildren<Canvas>();
		bigPlane = GetComponentInChildren<RawImage> ();
		GameObject[] temp = GameObject.FindGameObjectsWithTag("SecurityCamera");
		securityCameras = new Camera[temp.Length];
		renderTextures = new RenderTexture[temp.Length];
		for (int i = 0; i < temp.Length; i++) {
			securityCameras[i] = temp[i].GetComponentInChildren<Camera>();
		}

		for (int i = 0; i < securityCameras.Length; i++) {
			renderTextures[i] = new RenderTexture(256, 256, 0);
			securityCameras[i].targetTexture = renderTextures[i];
		}

		for (int i = 0; i < renderTextures.Length; i++) {
			GameObject camDisplay = (GameObject)Instantiate(securityCamDisplayPrefab, Vector3.zero, Quaternion.identity);
			camDisplay.name = "Camera Plane " + (i + 1f);
			camDisplay.transform.parent = canvas.transform;
			camDisplay.GetComponent<RawImage>().texture = renderTextures[i];
			camDisplay.GetComponent<RectTransform>().localPosition = new Vector3(-146 + (i*camDisplayBuffer), -140, 0);
			camDisplay.GetComponent<RectTransform>().localRotation = Quaternion.Euler(Vector3.zero);
			camDisplay.GetComponent<RectTransform>().localScale = Vector3.one;
			camDisplay.GetComponent<Button>().onClick.AddListener( delegate { OnClickCamDisplay(camDisplay); });
		}
	}

	void Update() {
		MoveCamera();
	}

	public void OnClickCamDisplay (GameObject camDisplay) {
		Texture temp = camDisplay.GetComponent<RawImage>().texture;
		camDisplay.GetComponent<RawImage>().texture = bigPlane.texture;
		bigPlane.texture = temp;
	}

	void MoveCamera () {
		mousePos = staticCam.ScreenToWorldPoint(Input.mousePosition);
		mousePos.y = 4;
		mousePosDelta = mousePos - previousMousePos;

		#region Camera Zoom
		if (Mathf.Abs(Input.mouseScrollDelta.y) > 0) {
			if (cam.orthographicSize <= (cameraSizeMin + Input.mouseScrollDelta.y)) {
				cam.orthographicSize = cameraSizeMin;
			} else if (cam.orthographicSize >= (cameraSizeMax + Input.mouseScrollDelta.y)) {
				cam.orthographicSize = cameraSizeMax;
			} else {
				cam.orthographicSize -= Input.mouseScrollDelta.y;
			}
		}
		#endregion

		#region Camera Move
		if (Input.GetMouseButton(1)) {
			Vector3 pos = cam.transform.position;
			pos -= mousePosDelta;
			cam.transform.position = pos;
		}

		previousMousePos = mousePos;
		previousMousePos.y = 4;
		#endregion
	}
}
