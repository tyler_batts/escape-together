﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

[RequireComponent(typeof (CharacterController))]
public class MovementController : MonoBehaviour {

	[SerializeField] private CameraLook cameraLook;
	[SerializeField] private PlayerStats playerStats = new PlayerStats();
	[SerializeField] private float stickToGroundForce, gravityMultiplier;

	private CharacterController characterController;
	private CollisionFlags collisionFlags;
	private MultiPlatformInput inputController;

	private Vector2 input;
	private Vector3 moveDir;
	private bool jump, isJumping, isRunning, isSprinting;
	private float speed;

	void OnEnable()
	{
		inputController = new MultiPlatformInput();
		characterController = GetComponent<CharacterController>();
		cameraLook.Init(transform, Camera.main.transform);
	}

	void Update() {
		RotateView();

		if (inputController.GetButtonDown("Jump")) {
			jump = true;
		}

		if (characterController.isGrounded) {
			isJumping = false;
		}
	}

	void FixedUpdate() {
		speed = 0;
		GetInput(out speed);

		// always move along the camera forward as it is the direction that it being aimed at
		Vector3 desiredMove = transform.forward*(Input.GetAxis("Joystick Vertical")) + transform.right*(Input.GetAxis("Joystick Horizontal"));

		// get a normal for the surface that is being touched to move along it
		RaycastHit hitInfo;
		Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo,
			characterController.height/2f, ~0, QueryTriggerInteraction.Ignore);
		desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

		moveDir.x = desiredMove.x*speed;
		moveDir.z = desiredMove.z*speed;

		if (characterController.isGrounded) {
			moveDir.y = -stickToGroundForce;
			if (jump) {
				Jump();
			}
		} else {
			moveDir += Physics.gravity*gravityMultiplier*Time.fixedDeltaTime;
		}
		collisionFlags = characterController.Move(moveDir*Time.fixedDeltaTime);
		cameraLook.UpdateCursorLock();
	}

	public void Jump () {
		moveDir.y = playerStats.jumpSpeed;
		jump = false;
		isJumping = true;
	}

	private void GetInput(out float speed)
	{
		// Read input
		float horizontal = Input.GetAxis("Joystick Horizontal");
		float vertical = Input.GetAxis("Joystick Vertical");

		bool waswalking = isRunning;

		#if !MOBILE_INPUT
		// On standalone builds, walk/run speed is modified by a key press.
		// keep track of whether or not the character is walking or running
		isRunning = !Input.GetButton("Joystick Sprint");
		if ((inputController.GetButton("Sprint") && inputController.GetAxis("Vertical") > 0)) {
			isRunning = false;
		}
		isSprinting = !isRunning;
		#endif
		// set the desired speed to be walking or running
		speed = isRunning ? playerStats.runSpeed : playerStats.sprintSpeed;
		input = new Vector2(horizontal, vertical);

		// normalize input if it exceeds 1 in combined length:
		if (input.sqrMagnitude > 1)
		{
			input.Normalize();
		}
	}

	private void RotateView()
	{
		cameraLook.LookRotation (transform, Camera.main.transform);
	}

	#region Properties
	public bool CursorIsLocked {
		get {
			return cameraLook.CursorIsLocked;
		}

		set {
			cameraLook.CursorIsLocked = value;
		}
	}

	public bool GetJump {
		get {
			return jump;
		}
	}

	public bool IsJumping {
		get {
			return isJumping;
		}
	}

	public float RunSpeed {
		get {
			return playerStats.runSpeed;
		}
	}

	public float GravityMultiplier {
		get {
			return gravityMultiplier;
		}
	}

	public Vector3 MoveDir {
		get {
			return moveDir;
		}
	}
	#endregion
}