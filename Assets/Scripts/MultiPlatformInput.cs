﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MultiPlatformInput {

	[SerializeField]
	bool controllerConnected;

	public void DebugMethod () {
		#if UNITY_EDITOR || DEVELOPMENT_BUILD
		if (Input.GetJoystickNames()[0].ToString() == "") {
			controllerConnected = false;
		} else {
			controllerConnected = true;
		}
		#endif
	}

	public bool isControllerConnected {
		get {
			if (Input.GetJoystickNames().Length == 0) { // no controller connected
				return false;
			} else if (Input.GetJoystickNames().Length == 1) {
				if (Input.GetJoystickNames()[0].ToString() == "") { // no controller connected
					return false;
				} else { // controller connected
					return true;
				}
			} else { // controller connected
				return true;
			}
		}
	}

	public bool GetButtonDown (string button) {
		if (Input.GetJoystickNames().Length == 0) { // no controller connected
			if (Input.GetButtonDown(button)) {
				return true;
			} else {
				return false;
			}
		} else if (Input.GetJoystickNames().Length == 1) {
			if (Input.GetJoystickNames()[0].ToString() == "") { // no controller connected
				if (Input.GetButtonDown(button)) {
					return true;
				} else {
					return false;
				}
			} else { // controller connected
				if (Input.GetButtonDown("Joystick " + button)) {
					return true;
				} else {
					return false;
				}
			}
		} else { // controller connected
			if (Input.GetButtonDown("Joystick " + button)) {
				return true;
			} else {
				return false;
			}
		}
	}

	public bool GetButton (string button) {
		if (Input.GetJoystickNames()[0] == "") {
			if (Input.GetButton(button)) {
				return true;
			} else {
				return false;
			}
		} else {
			if (button == "Sprint") {
				if (Input.GetAxis("Joystick Sprint") > 0.5f)
					return true;
				else
					return false;
			}

			if (Input.GetButton("Joystick " + button)) {
				return true;
			} else {
				return false;
			}
		}
	}

	public bool GetButtonUp (string button) {
		if (Input.GetJoystickNames()[0] == "") {
			if (Input.GetButtonUp(button)) {
				return true;
			} else {
				return false;
			}
		} else {
			if (Input.GetButtonUp("Joystick " + button)) {
				return true;
			} else {
				return false;
			}
		}
	}

	public float GetAxis (string axis) {
		if (Input.GetJoystickNames()[0] == "") {
			return Input.GetAxis(axis);
		} else {
			return Input.GetAxis("Joystick " + axis);
		}
	}
}
