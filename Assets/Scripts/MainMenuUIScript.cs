﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class MainMenuUIScript : MonoBehaviour {
	
	[SerializeField] GameObject exitPopup;
	[SerializeField] Button playButton, exitButton;

	public void Play() {
		SceneManager.LoadScene("1_LevelEscape");
	}

	public void Credits() {
		SceneManager.LoadScene("99_Credits");
	}

	public void ExitPopup() {
		if (!exitPopup.activeInHierarchy) {
			exitPopup.SetActive(true);
			playButton.interactable = false;
			exitButton.interactable = false;
		}
	}

	public void ExitPopupConfirm() {
		Application.Quit();
	}

	public void ExitPopupCancel() {
		if (exitPopup.activeInHierarchy) {
			exitPopup.SetActive(false);
			playButton.interactable = true;
			exitButton.interactable = true;
		}
	}
}
