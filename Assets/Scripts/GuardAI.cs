﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections;

public class GuardAI : MonoBehaviour {
	enum PathEndBehavior { Stop, Loop, Reverse };
	enum GuardState { Normal, Stunned };

	NavMeshAgent agent;
	Rigidbody rb;
	Ray raycast;
	RaycastHit hit;

	[SerializeField] private PathEndBehavior endBehavior;
	[SerializeField] private float delayLength;
	private bool reversing = false;
	private bool isDelaying;
	[SerializeField] private Vector3[] pathPoints;
	int targetIndex = 0;

	private bool playerSpotted;
	private GameObject player;
	private Vector3 lastKnownPlayerPos;
	[SerializeField] private float raycastDistance;
	[SerializeField] float searchTime, stopDistance;
	[SerializeField] float alertedSpeed, alertedRotationalSpeed;
	[SerializeField] float idleSpeed, idleRotationalSpeed;
	[SerializeField] float stunLength;
	private GuardState guardState;
	private Animator animator;

	void Start () {
		rb = GetComponent<Rigidbody>();
		agent = GetComponent<NavMeshAgent>();
		animator = GetComponentInChildren<Animator>();
		player = GameObject.FindGameObjectWithTag ("Player");

		int tempNode = 0;
		float tempDistance = 9999f;
		for (int i = 0; i < pathPoints.Length; i++) {
			if ((pathPoints [i] - transform.position).magnitude < tempDistance)
				tempNode = i;
		}
		agent.SetDestination(pathPoints[tempNode]);
	}

	void Update () {
		if (GetComponentInChildren<Button> ().interactable) {
			Color temp = GetComponentInChildren<Image>().color;
			temp.a = 0.5f;
			GetComponentInChildren<Image>().color = temp;
		}

		if (targetIndex < 0)
			targetIndex = 0;
		RunTimer ();
		RunTimerStun ();

		#region Player Spotted
		if (guardState == GuardState.Stunned) {
			animator.SetBool("Stunned", true);
			StartTimerStun(false);
			agent.Stop();
			Color temp = GetComponentInChildren<Image>().color;
			temp.a = 0.5f;
			GetComponentInChildren<Image>().color = temp;

			if (timerStun >= stunLength) {
				guardState = GuardState.Normal;
				StopTimerStun(true);
				agent.Resume();
			}
		} else if (guardState == GuardState.Normal) {
			animator.SetBool("Stunned", false);
			if (playerSpotted) {
				Physics.BoxCast(transform.position, new Vector3(1.5f, 0.5f, 0.5f), (transform.rotation * Vector3.forward), out hit, Quaternion.identity, raycastDistance);

				agent.speed = alertedSpeed;
				agent.angularSpeed = alertedRotationalSpeed;
				agent.SetDestination(lastKnownPlayerPos);
				if (hit.collider != null) {
					if (hit.collider.tag == "Player") {
						lastKnownPlayerPos = hit.collider.transform.position - (hit.collider.transform.position - transform.position).normalized*stopDistance;
						lastKnownPlayerPos.y = 1.0625f;
						StartTimer(true);
					} else {
						playerSpotted = false;
					}
				} else {
					if (timer < searchTime) {
						lastKnownPlayerPos = player.transform.position - (player.transform.position - transform.position).normalized*stopDistance;
						lastKnownPlayerPos.y = 1.0625f;
					} else {
						playerSpotted = false;
						StopTimer(false);
						FollowPath();
					}
				}
			} else {
				if (isTiming && timer < searchTime) {
					lastKnownPlayerPos = player.transform.position - (player.transform.position - transform.position).normalized*stopDistance;
					lastKnownPlayerPos.y = 1.0625f;
					agent.SetDestination(lastKnownPlayerPos);
				} else {
					agent.speed = idleSpeed;
					agent.angularSpeed = idleRotationalSpeed;
					StopTimer(false);
					agent.SetDestination(pathPoints[targetIndex]);
					Move();
				}
			}
		}
		#endregion
	}

	void Move() {
		FollowPath();
		Physics.BoxCast(transform.position, new Vector3(1.5f, 0.5f, 0.5f), (transform.rotation * Vector3.forward), out hit, Quaternion.identity, raycastDistance);
		if (!playerSpotted) {
			if (hit.collider != null) {
				if (hit.collider.tag == "Player") {
					playerSpotted = true;
					//lastKnownPlayerPos = player.transform.position;
					GetComponentInChildren<Image> ().color = Color.red;
				} else {
					playerSpotted = false;
					GetComponentInChildren<Image> ().color = Color.green;
				}
			} else {
				playerSpotted = false;
				GetComponentInChildren<Image> ().color = Color.green;
			}
		}
	}

	#region Pathing
	void FollowPath() {
		if (transform.position == pathPoints [targetIndex]) {
			if (targetIndex == 0 && reversing) {
				reversing = false;
			} else if (targetIndex < pathPoints.Length - 1) {
				if (reversing) {
					StartCoroutine(MoveReverse());
				} else {
					StartCoroutine(MoveForward(false));
				}
			} else {
				if (endBehavior == PathEndBehavior.Reverse) {
					StartCoroutine(MoveReverse());
					reversing = true;
				} else if (endBehavior == PathEndBehavior.Loop) {
					StartCoroutine(MoveForward(true));
				}
			}
		}
	}

	IEnumerator MoveForward (bool restart) {
		targetIndex = restart ? 0 : targetIndex+1;
		isDelaying = true;
		yield return new WaitForSeconds(delayLength);
		isDelaying = false;
		agent.SetDestination (pathPoints [targetIndex]);
	}

	IEnumerator MoveReverse() {
		targetIndex--;
		isDelaying = true;
		yield return new WaitForSeconds(delayLength);
		isDelaying = false;
		agent.SetDestination (pathPoints [targetIndex]);
	}
	#endregion

	#region Timer
	bool isTiming;
	float timer;
	void StartTimer (bool reset) {
		isTiming = true;
		if (reset)
			timer = 0;
	}

	void RunTimer () {
		if (isTiming)
			timer += Time.deltaTime;
	}

	void StopTimer(bool reset) {
		isTiming = false;
		if (reset)
			timer = 0;
	}

	bool isTimingStun;
	float timerStun;
	void StartTimerStun (bool reset) {
		isTimingStun = true;
		if (reset)
			timerStun = 0;
	}

	void RunTimerStun () {
		if (isTimingStun)
			timerStun += Time.deltaTime;
	}

	void StopTimerStun(bool reset) {
		isTimingStun = false;
		if (reset)
			timerStun = 0;
	}
	#endregion

	#region Button Click
	public void OnUIButtonClick () {
		if (guardState != GuardState.Stunned) {
			guardState = GuardState.Stunned;
		}
	}
	#endregion

	void OnDrawGizmos() {
		Vector3 pos = transform.position;
		Gizmos.color = Color.green;
		Gizmos.color = Color.red;
		for (int i = 0; i < pathPoints.Length; i++) { //Draw Path
			Gizmos.DrawSphere(pathPoints[i], 0.25f);
			if (i == pathPoints.Length-1) {
				if (endBehavior == PathEndBehavior.Loop) {
					Gizmos.DrawLine (pathPoints [i], pathPoints [0]);
				}
			} else {
				Gizmos.DrawLine(pathPoints[i], pathPoints[i+1]);
			}
		}

		Gizmos.color = Color.green;
		Gizmos.DrawSphere(lastKnownPlayerPos, 0.25f);
	}

	void OnCollisionEnter(Collision other) {
		if (other.collider.tag == "Door") {
			other.gameObject.GetComponentInParent<DoorControl> ().ActivateControl ();
		}
	}
}
